#!/usr/bin/python
import sys
import getopt
try:
	import numpy as np
except ImportError:
	print "Install NumPy!"
	sys.exit(2)
try:
	import matplotlib.pyplot as plt
	import matplotlib.cm as cm
except ImportError:
	print "Install matplotlib!"
	sys.exit(2)
try:
	from scipy import ndimage
except ImportError:
	print "Install SciPy!"
	sys.exit(2)


def filter_isolated_cells(array, struct):
	""" Return array with completely isolated single cells removed
	:param array: Array with completely isolated single cells
	:param struct: Structure array for generating unique regions
	:return: Array with minimum region size > 1
	"""

	filtered_array = np.copy(array)
	id_regions, num_ids = ndimage.label(filtered_array, structure=struct)
	id_sizes = np.array(ndimage.sum(array, id_regions, range(num_ids + 1)))
	area_mask = (id_sizes == 1)
	filtered_array[area_mask[id_regions]] = 0
	return filtered_array


def fill_vertexes(arr):
	sh = arr.shape
	sh = (sh[0] - 1, sh[1] - 1)
	for (x,y), value in np.ndenumerate(arr):
		if value == 0 and x > 0 and y > 0 and x < sh[0] and y < sh[1] and\
			(
				(int(arr[x-1,y]) == 1 and int(arr[x-1,y-1]) == 1 and\
				 int(arr[x,y-1]) == 1 and int(arr[x+1,y-1]) == 0 and\
				 int(arr[x+1,y]) == 0 and int(arr[x+1,y+1]) == 0 and\
				 int(arr[x,y+1]) == 0 and int(arr[x-1,y+1]) == 0) or\
				(int(arr[x-1,y]) == 0 and int(arr[x-1,y-1]) == 0 and\
				 int(arr[x,y-1]) == 1 and int(arr[x+1,y-1]) == 1 and\
				 int(arr[x+1,y]) == 1 and int(arr[x+1,y+1]) == 0 and\
				 int(arr[x,y+1]) == 0 and int(arr[x-1,y+1]) == 0) or\
				(int(arr[x-1,y]) == 0 and int(arr[x-1,y-1]) == 0 and\
				 int(arr[x,y-1]) == 0 and int(arr[x+1,y-1]) == 0 and\
				 int(arr[x+1,y]) == 1 and int(arr[x+1,y+1]) == 1 and\
				 int(arr[x,y+1]) == 1 and int(arr[x-1,y+1]) == 0) or\
				(int(arr[x-1,y]) == 1 and int(arr[x-1,y-1]) == 0 and\
				 int(arr[x,y-1]) == 0 and int(arr[x+1,y-1]) == 0 and\
				 int(arr[x+1,y]) == 0 and int(arr[x+1,y+1]) == 0 and\
				 int(arr[x,y+1]) == 1 and int(arr[x-1,y+1]) == 1)
			):
			arr[x,y] = 1
	return arr


def get_holes(orig, closed, max, x, y):
	minx = x - max
	miny = y - max
	starty = y
	holes = []
	while x > minx:
		while y > miny:
			if orig[x,y] == 0 and closed[x,y] == 1:
				holes.append((x,y))
			y -= 1
		x -= 1
		y = starty
	return holes

	
def sub_matrix(orig):
	arr = np.zeros(orig.shape, dtype=int)
	arr[:,0] = orig[:,0]
	for (x,y), value in np.ndenumerate(orig):
		if x > 0 and y > 0:
			if value == 1:
				arr[x,y] = min(arr[x-1,y-1], min(arr[x,y-1], arr[x-1,y])) + 1
			else:
				arr[x,y] = int(0)
	
	max = 0
	max_x, max_y = 0,0
	for (x,y), value in np.ndenumerate(arr):
		if value > max and value % 2 == 1:
			max, max_x, max_y = value, x, y
	return arr, max, (max_x, max_y)


def draw_square(arr, max, x, y):
	minx = x - max
	miny = y - max
	starty = y
	while x > minx:
		while y > miny:
			arr[x,y] = 0
			y -= 1
		x -= 1
		y = starty


def process(filename):
	try:
		f = open(filename, 'r')
	except IOError:
		print "File %s does not exists!" % filename
		sys.exit(2)
	fl = f.readline()
	fl.strip('\r').strip('n').split(" ")
	arr = None
	for line in f:
		v = np.array([0 if x == '.' else 1 for x in line.strip('\n').strip('\r')])
		arr = np.vstack([arr, v]) if arr is not None else np.array(v, ndmin=2)
	f.close()
	plt.imsave('%s.png' % filename, arr, cmap=cm.gray)
	
	operations = []
	
	max = sys.maxint
	sq_min = 3
	i = 0
	while max >= sq_min:
		sub, max, (sm_x,sm_y) = sub_matrix(arr)
		
		holes = []
	
		closed = arr.copy()
		closed = ndimage.binary_fill_holes(closed)
		closed = fill_vertexes(closed)
		sub_c, max_c, (sm_x_c,sm_y_c) = sub_matrix(closed)
		out = open('%s-closed%d.txt' % (filename, i), 'w')
		for (x,y), value in np.ndenumerate(closed):
			if y == 0:
				out.write('\n')
			out.write('.' if value == 0 else '#')
		out.close()
		
		if max_c > max:
			sub = sub_c
			max = max_c
			sm_x = sm_x_c
			sm_y = sm_y_c
			holes = get_holes(arr, closed, max, sm_x, sm_y)
		
		if max >= sq_min:
			print '%d %d %d' % (max, sm_x, sm_y)
			draw_square(arr, max, sm_x, sm_y)
			dim = int((max - 1) / 2)
			x = sm_x - dim
			y = sm_y - dim
			operations.append('PAINT_SQUARE %d %d %d' % (x, y, dim))
			for h in holes:
				operations.append('ERASE_CELL %d %d' % (h[0], h[1]))
			# np.savetxt('%s-sub%d.txt' % (filename, i), sub, fmt='%d')
			out = open('%s-sub%d.txt' % (filename, i), 'w')
			for (x,y), value in np.ndenumerate(arr):
				if y == 0:
					out.write('\n')
				out.write('.' if value == 0 else '#')
			out.close()
			plt.imsave('%s-sub%d.png' % (filename, i), arr, cmap=cm.gray)
			i += 1
	labels, n = ndimage.measurements.label(arr, np.ones((3, 3)))
	bboxes = ndimage.measurements.find_objects(labels)
	
	f = open('%s.out' % filename, 'w')
	f.write('%d\n' % len(operations))
	for op in operations:
		f.write('%s\n' % op)
	f.close()


def main(argv):
	# parse command line options
	if len(argv) != 1:
		print "Use to_image.py [filename]"
		sys.exit(2)
	f = argv[0]
	process(f)

if __name__ == "__main__":
    main(sys.argv[1:])
