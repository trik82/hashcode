import os
import sys


def process(filename):
    try:
        f = open(filename, 'r')

        rows, columns, drones_num, max_iterations, max_load = (int(x) for x in f.readline().split(' '))
        print "Rows: %d" % rows
        print "Columns: %d" % columns
        print "Drones: %d" % drones_num
        print "Max iterations: %d" % max_iterations
        print "Max drone load: %d" % max_load
        print ""

        num_products = int(f.readline())

        product_weights = [int(x) for x in f.readline().split(' ')]
        print "Products: %d" % num_products

        num_warehouses = int(f.readline())
        print "Warehouses: %d" % num_warehouses

        warehouses = []

        for i in xrange(0, num_warehouses):
            warehouses.append({
                'position': [int(x) for x in f.readline().split(' ')],
                'products': [int(x) for x in f.readline().split(' ')]
            })

        num_orders = int(f.readline())

        orders = []
        print "Orders: %d" % num_orders

        for i in xrange(0, num_orders):
            orders.append({
                'destination': [int(x) for x in f.readline().split(' ')],
                'items_num': int(f.readline()),
                'items': [int(x) for x in f.readline().split(' ')]
            })

    except IOError:
        print "File %s does not exists!" % filename
        sys.exit(2)


def main(argv):
    # parse command line options
    if len(argv) != 1:
        print "Use delivery.py [filename]"
        sys.exit(2)
    f = argv[0]
    process(f)

if __name__ == "__main__":
    main(sys.argv[1:])
